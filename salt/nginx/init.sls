# Install a "vanilla" version of nginx from system packages

install-nginx:
  pkg.installed:
    - name: nginx

service-nginx:
  service:
    - name: nginx
    - enable: True
    - reload: True
    - running
    - require:
      - pkg: nginx
