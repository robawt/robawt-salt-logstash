# Logstash 
#   Written for v 1.2.1 in mind
#   Double check pillar/logstash.sls for version changes.

# This is both the user and the group "block" for logstash
include:
  - redis-server
  - elasticsearch

logstash-home:
  file.directory:
    - name: {{ pillar['logstash']['flatjar']['path'] }}

# Add user & group :logstash/logstash
logstash-user:
  group.present:
    - name: logstash
    - system: True
  user.present:
    - name: logstash
    - system: True
    - groups:
      - logstash
    - home: /opt/logstash
    - require:
      - file: logstash-home

# Add logstash config $HOME directory
logstash-config-directory:
    file.directory:
      - name: /etc/logstash
      - require: 
        - file: logstash-home

# Jar file
logstash-file:
  file.managed:
    - name: {{ pillar['logstash']['flatjar']['path'] }}/logstash-{{ pillar['logstash']['flatjar']['version'] }}-flatjar.jar
    - source: salt://logstash/logstash-{{ pillar['logstash']['flatjar']['version'] }}-flatjar.jar
    - require: 
      - file: logstash-home

# Logstash indexer config file
logstash-config:
  file.managed:
    - name: /etc/logstash/indexer.conf
    - source: salt://logstash/config/indexer.conf
    - require:
      - file: logstash-config-directory

# Logstash indexer upstart script (for services)
#   REQUIRES user: logstash
logstash-indexer-service:
  file.managed:
    - name: /etc/init/logstash-indexer.conf
    - source: salt://logstash/service/logstash-indexer.conf
    - template: jinja
    - require:
      - user: logstash

